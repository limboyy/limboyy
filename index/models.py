from django.db import models

# Create your models here.
class Index_title(models.Model):
    uName = models.CharField(max_length=20)
    imply = models.CharField(max_length=20)
    key = models.CharField(max_length=20)
    dateTime = models.DateTimeField(auto_now=True)

class Index_text(models.Model):
    userId = models.IntegerField()
    text = models.TextField()
    dateTime = models.DateTimeField(auto_now=True)
    imgPath = models.CharField(max_length=40)

class Chose(models.Model):
    k = models.IntegerField()


class LoginTime(models.Model):
    dateTime = models.DateTimeField(auto_now=True)
    key = models.CharField(max_length=20)

class Liwu(models.Model):
    dateTime = models.DateTimeField(auto_now=True)
    chose = models.CharField(max_length=40)