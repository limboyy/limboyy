from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path(r'', views.index, name='index'),
    path(r'index_next/', views.index_next, name='index_next'),
    path(r'keyIf/', views.keyIf, name='keyIf'),
    path(r'webIntro/', views.webIntro, name='webIntro'),
    path(r'button/', views.button, name='button'),
    path(r'liwuchose/', views.liwuchose, name="liwuchose")
]