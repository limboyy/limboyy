from django.shortcuts import render, redirect, reverse
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from .models import *
from functools import wraps


def check_login(f):
    @wraps(f)
    def inner(requeset, *args, **kwargs):
        if requeset.session.get('is_login') == requeset.GET.get("id"):
            return f(requeset, *args, **kwargs)
        else:
            return redirect("/index/")
    return inner
def index(request):
    if request.method == 'POST':
        uName = request.POST.get('uName')
        key = request.POST.get('key')
        imply = request.POST.get('imply')
        userInfor = Index_title(uName=uName, key=key, imply=imply)
        userInfor.save()
        return JsonResponse({"status": "True", "id": userInfor.id})
    titleAll = Index_title.objects.all()
    # content = {'titleAll': titleAll}
    return render(request,'index/index.html', {'titleAll': titleAll})

def button(request):
    if request.method == 'POST':
        button = request.POST.get('button')
        k = request.POST.get('k')
        print(button)
        chose = Chose(k=k)
        chose.save()
        return JsonResponse({"status": "True"})

# @check_login
def index_next(request):
    userId = request.GET.get('id')
    if request.method == "POST":
        text_id = request.POST.get('text_id')
        if text_id == '':
            text_id = -1
        text = request.POST.get('text')
        userId = request.POST.get('id')
        imgPath = request.POST.get('imgPath', "None")
        idRes = Index_text.objects.filter(id=text_id)
        if idRes:
            idRes[0].text = text
            idRes[0].save()
            return JsonResponse({"status": "True"})
        else:
            textInfor = Index_text(userId=userId, text=text, imgPath=imgPath)
            textInfor.save()
            return JsonResponse({"status": "True"})
    textAll = Index_text.objects.filter(userId=userId)
    return render(request,'index/index_next.html', {'textAll': textAll})


def keyIf(request):
    if request.method == "POST":
        title_id = request.POST.get('title_id')
        title_key = request.POST.get('title_key')
        login_time = LoginTime(key=title_key)
        login_time.save()
        res = Index_title.objects.filter(id=title_id, key=title_key)
        if res:
            request.session["is_login"] = title_id
            return JsonResponse({'url': '/index/index_next/?id=%s' % title_id, 'status': 'True'})
        else:
            return JsonResponse({"status": "False"})


def liwuchose(request):
    if request.method == 'POST':
        chose = request.POST.get('chose')
        print(chose)
        liwu = Liwu(chose=chose)
        liwu.save()
        return JsonResponse({"status": "True"})


def webIntro(request):
    # with open("", 'r')
    pass
